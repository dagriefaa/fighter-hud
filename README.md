<div align="center">
<img src="header.png" alt="AC HUD MOD" width="500"/><br>
https://steamcommunity.com/sharedfiles/filedetails/?id=1498426754<br>
A mod for [Besiege](https://store.steampowered.com/app/346010), a physics based building sandbox game. <br><br>

![Steam Views](https://img.shields.io/steam/views/1498426754?color=%231b2838&logo=steam&style=for-the-badge)
![Steam Subscriptions](https://img.shields.io/steam/subscriptions/1498426754?color=%231b2838&logo=steam&style=for-the-badge)
![Steam Favorites](https://img.shields.io/steam/favorites/1498426754?color=%231b2838&logo=steam&style=for-the-badge)
</div>

Ace Combat Heads-Up Display is a mod for [Besiege](https://store.steampowered.com/app/346010) which adds a fully-featured and highly configurable aircraft heads-up display (inspired by Ace Combat 7's HUD) to camera blocks.

Developed in collaboration with **Shadé**, who pitched the concept, assisted with design, and created the graphical assets.

## Features
Displays the following information out-of-the-box:
- Vehicle speed, altitude, and heading
- An artificial horizon
- Simulation time and level progress
- Custom level completion screen
- Targeting and tracking of other players
- Minimap that shows level boundaries (and targets)

Displays the following with extra configuration:
- Allows targeting/tracking level entities
- Ammunition readouts (missiles, bombs, guns, etc)
- Throttle position
- Landing gear retraction

## Technologies
* Unity 5.4 - the game engine Besiege runs on
* C# 10/.NET Framework 3.5 - the environment Unity 5.4 shipped with

## Installation
The latest version of the mod can be found on the Steam Workshop, and can be installed at the press of the big green 'Subscribe' button. It will automatically activate the next time you boot the game.

If, for whatever reason, you don't want to use Steam to manage your mods, installation is as follows:
1. Install Besiege.
2. Clone the repository into `Besiege/Besiege_Data/Mods`.
3. Open the `.sln` file in `src` in Visual Studio 2015 (or later).
4. Press F6 to compile the mod.

## Usage
To get started, place a camera block and set the 'Enable AC HUD' toggle on. This will display the HUD for that camera block.
- The 'HUD: Attitude Display' option shows/hides the attitude display (for third-person views).

Ammunition readouts (for missiles/bombs) are set with the HUD menu on grabbers or decouplers. It can be set to 'HUD: None', 'HUD: Missile', 'HUD: Bomb', and 'HUD: Other'. <br>
Missiles on the ammo display are ordered first left-to-right (X axis) then forward-backward (Z axis).

The throttle readout can run in two modes:
- With the 'HUD: Throttle' settings on steering hinges (Continuous). There are options for both directions; if it's moving the wrong way, use the other option.
- With the keybinds on the camera for 'Engine' and 'Throttle' (Discrete).

The landing gear readout is configured with the HUD menu on steering hinges, with the 'HUD: Landing Gear' option.

Level entities can be configured for display by adding the following to the end of the entity's name: <br>
HUD_<type\>_<alignment\>_<priority\>
- type: a|g|i|null - air, ground, invincible, or nothing
- alignment: enm|tgt|fnd|neu - enemy, target enemy, ally, neutral
- priority: pri|null - sticks a larger box on top of the entity; can be used in conjunction with a null type for landmarks etc.

Additional usage instructions can be found on the [Steam Workshop page](https://steamcommunity.com/sharedfiles/filedetails/?id=1498426754).

## Status
This mod is feature-complete. <br>
However, additional features and fixes may be added in the future as necessary.

## Images
<img src="https://i.imgur.com/EQRdZCn.png" alt="showcase" width="500"/>
<img src="https://i.imgur.com/NAM3x5X.jpg" alt="entities" width="500"/>
