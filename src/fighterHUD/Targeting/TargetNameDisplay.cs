﻿using UnityEngine;
using UnityEngine.UI;

/*
 * Displays the name of the current targeted entity.
 */
namespace FighterHUD;
public class TargetNameDisplay : MonoBehaviour {

    Text targetText = null;

    // Use this for initialization
    void Start() {
        targetText = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update() {
        targetText.enabled = EntityDisplayController.CurrentTarget != null;
        targetText.text = "TARGET  " + EntityDisplayController.CurrentTarget?.EntityName;
    }
}
