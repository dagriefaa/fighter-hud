﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

namespace FighterHUD;
public class TargetPointerController : MonoBehaviour {

    Transform textTransform;
    Text nameText;
    Text priorityText;
    Transform pivot;

    void Awake() {
        textTransform = CameraHUDController.Screen.FindChild("Target Pointer Text");
        nameText = textTransform.transform.FindChild("Name").GetComponent<Text>();
        priorityText = textTransform.transform.FindChild("Target").GetComponent<Text>();
        pivot = this.transform.FindChild("Pivot");
    }

    void Update() {
        if (!CameraHolder.CurrentCamera) return;

        if (!EntityDisplayController.CurrentTarget || EntityDisplayController.CurrentTarget.InFrontOfCamera) {
            foreach (Transform t in this.transform) {
                t.gameObject.SetActive(false);
            }
            textTransform.gameObject.SetActive(false);
        }
        else {
            foreach (Transform t in this.transform) {
                t.gameObject.SetActive(true);
            }
            this.transform.LookAt(EntityDisplayController.CurrentTarget.SelfTransform);
            textTransform.position = pivot.position;
            textTransform.localPosition = new Vector3(textTransform.localPosition.x, textTransform.localPosition.y, 0f);
            textTransform.gameObject.SetActive(true);
            nameText.text = EntityDisplayController.CurrentTarget.EntityName;
            priorityText.gameObject.SetActive(EntityDisplayController.CurrentTarget.IsTarget);
        }

    }
}
