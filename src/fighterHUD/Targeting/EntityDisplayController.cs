﻿using Modding;
using Modding.Blocks;
using Modding.Levels;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/**
 * Display controller for an individual entity.
 */
namespace FighterHUD;
public class EntityDisplayController : MonoBehaviour {

    static Sprite airScreenImage;
    static Sprite airScreenTargetImage;
    static Sprite airMinimapImage;
    static Sprite groundScreenImage;
    static Sprite groundScreenTargetImage;
    static Sprite groundMinimapImage;
    static Sprite invincibleScreenImage;
    static Sprite invincibleScreenTargetImage;
    static Sprite invincibleMinimapImage;

    public static void SetupResources() {
        airScreenImage = ModResource.GetTexture("UnitAir.Screen").ToSprite();
        airScreenTargetImage = ModResource.GetTexture("UnitAir.ScreenTarget").ToSprite();
        airMinimapImage = ModResource.GetTexture("UnitAir.Map").ToSprite();
        groundScreenImage = ModResource.GetTexture("UnitGround.Screen").ToSprite();
        groundScreenTargetImage = ModResource.GetTexture("UnitGround.ScreenTarget").ToSprite();
        groundMinimapImage = ModResource.GetTexture("UnitGround.Map").ToSprite();
        invincibleScreenImage = ModResource.GetTexture("UnitInvincible.Screen").ToSprite();
        invincibleScreenTargetImage = ModResource.GetTexture("UnitInvincible.ScreenTarget").ToSprite();
        invincibleMinimapImage = ModResource.GetTexture("UnitInvincible.Map").ToSprite();
    }

    static Sprite TargetImage(EntityType type, bool targeted) => type switch {
        EntityType.Air => targeted ? airScreenTargetImage : airScreenImage,
        EntityType.Ground => targeted ? groundScreenTargetImage : groundScreenImage,
        EntityType.Invincible => targeted ? invincibleScreenTargetImage : invincibleScreenImage,
        EntityType.Hidden => null,
        _ => throw new NotImplementedException()
    };

    static Sprite MinimapImage(EntityType type) => type switch {
        EntityType.Air => airMinimapImage,
        EntityType.Ground => groundMinimapImage,
        EntityType.Invincible => invincibleMinimapImage,
        EntityType.Hidden => null,
        _ => throw new NotImplementedException()
    };

    public static List<EntityDisplayController> Entities = new List<EntityDisplayController>();
    public static EntityDisplayController CurrentTarget = null;

    // objects
    public GameObject OnMinimap { get; private set; } = null;
    public GameObject OnScreen { get; private set; } = null;
    public LevelEntity Self { get; private set; } = null;

    // properties 
    public Transform SelfTransform {
        get {
            if (Self.isBuildZone) {
                return (Self.behaviour as BuildZoneObject).buildZone.player.machine.SimulationBlocks[0].transform;
            }
            return Self.transform;
        }
    }

    public Vector2 ScreenPosition {
        get {
            RectTransform canvasRect = CameraHUDController.Screen as RectTransform;
            Vector2 viewportPosition = CameraHUDController.MainCamera.WorldToViewportPoint(SelfTransform.position);
            return new Vector2(
                        ((viewportPosition.x * canvasRect.sizeDelta.x) - (canvasRect.sizeDelta.x * 0.5f)),
                        ((viewportPosition.y * canvasRect.sizeDelta.y) - (canvasRect.sizeDelta.y * 0.5f)));
        }
    }

    public bool InFrontOfCamera {
        get {
            //return Vector3.Dot((SelfTransform.position - CameraHUDController.MainCamera.transform.position).normalized,
            //    CameraHUDController.MainCamera.transform.forward) > 0 && Type != EntityType.Hidden;
            Vector3 viewportPosition = CameraHUDController.MainCamera.WorldToViewportPoint(SelfTransform.position);
            return viewportPosition.z > 0
                && viewportPosition.x > 0 && viewportPosition.x < 1
                && viewportPosition.y > 0 && viewportPosition.y < 1;
        }
    }

    public float ScreenCenterSquareDistance {
        get {
            if (Type == EntityType.Hidden || this.IsDead || !this.InFrontOfCamera) {
                return Mathf.Infinity;
            }
            return ScreenPosition.sqrMagnitude;
        }
    }

    public bool IsDead {
        get {
            if (Self.isBuildZone) {
                return !((Self.behaviour as BuildZoneObject).buildZone != null
                         && (Self.behaviour as BuildZoneObject).buildZone.player.machine.isSimulating
                         && PlayerMachine.GetLocal().Player.InternalObject != (Self.behaviour as BuildZoneObject).buildZone.player);
            }
            else {
                bool res = !Self.gameObject.activeSelf;
                if (Self.behaviour is AIGenericEntity) {
                    res |= (Self.behaviour as AIGenericEntity).aiEntity.isDead;
                }
                else if (Self.behaviour.physTile.Length > 0) {
                    res |= Self.behaviour.physTile[0].BrokenInstance != null;
                }
                return res;
            }
        }
    }

    public bool IsNextTarget {
        get {
            return CurrentTarget == null
                || CurrentTarget.IsDead
                || (CameraHUDController.GetNextTarget
                    && this.ScreenCenterSquareDistance < CurrentTarget.ScreenCenterSquareDistance);
        }
    }

    // enums
    public enum EntityType { Air, Ground, Invincible, Hidden }
    public enum EntityAlignment { Enemy, Ally, Neutral }

    // UI state
    public EntityType Type { get; private set; } = EntityType.Air;
    public EntityAlignment Alignment { get; private set; } = EntityAlignment.Enemy;
    public bool IsTarget { get; private set; } = false;
    public bool IsPriority { get; private set; } = false;
    public string EntityName { get; private set; }

    // UI elements
    public Image TypeImage { get; private set; }
    public Text NameText { get; private set; }
    public Text DistanceText { get; private set; }
    public Text TargetText { get; private set; }
    public Image PriorityImage { get; private set; }

    public Image MiniImage { get; private set; }
    public Image MiniTargetImage { get; private set; }
    public Image MiniPriorityImage { get; private set; }

    public static void From(Entity e) {
        if (e.IsBuildZone || e.Name.ToLowerInvariant().Contains("hud_")) {
            e.SimEntity.InternalObject.gameObject.AddComponent<EntityDisplayController>();
        }
    }

    void Start() {
        this.Self = GetComponent<LevelEntity>();

        LoadEntityConfig();

        CreateEntityDisplays();

        Entities.Add(this);

        SetupEntityDisplays();
    }

    void OnDestroy() {
        Entities.Remove(this);
        Destroy(OnScreen);
        Destroy(OnMinimap);
    }

    void LoadEntityConfig() {
        if (Self.isBuildZone) return;

        string[] args = Self.LogicName().ToLowerInvariant().Split('_');
        EntityName = args[0].Substring(0, args[0].Length - 3).ToUpper();

        switch (args[1]) { // type
            case "a":
                Type = EntityType.Air;
                break;
            case "g":
                Type = EntityType.Ground;
                break;
            case "i":
                Type = EntityType.Invincible;
                break;
            case "h":
                Type = EntityType.Hidden;
                break;
            default:
                Debug.Log($"[Camera HUD] Entity {EntityName} type incorrect, falling back to default");
                break;
        }

        IsTarget = false;
        if (args.Length > 2) { // alignment
            switch (args[2]) {
                case "enm":
                    Alignment = EntityAlignment.Enemy;
                    break;
                case "tgt":
                    Alignment = EntityAlignment.Enemy;
                    IsTarget = Type != EntityType.Hidden;
                    break;
                case "fnd":
                    Alignment = EntityAlignment.Ally;
                    break;
                case "neu":
                    Alignment = EntityAlignment.Neutral;
                    break;
                default:
                    Debug.Log($"[Camera HUD] Entity {EntityName} alignment incorrect, falling back to default");
                    break;
            }
        }

        IsPriority = (Type != EntityType.Hidden && args.Length > 3 && args[3] == "pri");

    }

    void CreateEntityDisplays() {
        OnMinimap = Instantiate(CameraHUDController.EntityMinimapPrefab);
        OnMinimap.transform.SetParent(CameraHUDController.Minimap, false);
        OnMinimap.transform.localRotation = Quaternion.identity;
        OnMinimap.transform.localScale = Vector3.one;

        MiniImage = OnMinimap.GetComponent<Image>();
        MiniTargetImage = OnMinimap.transform.FindChild("MiniTargetCritical").GetComponent<Image>();
        MiniPriorityImage = OnMinimap.transform.FindChild("MiniTargetPriority").GetComponent<Image>();

        OnScreen = Instantiate(CameraHUDController.EntityTargetPrefab);
        OnScreen.transform.SetParent(CameraHUDController.Screen, false);
        OnScreen.transform.localRotation = Quaternion.identity;
        OnScreen.transform.localScale = Vector3.one;

        TypeImage = OnScreen.transform.FindChild("ScreenTargetType").GetComponent<Image>();
        NameText = OnScreen.transform.FindChild("ScreenTargetName").GetComponent<Text>();
        DistanceText = OnScreen.transform.FindChild("ScreenTargetDistance").GetComponent<Text>();
        TargetText = OnScreen.transform.FindChild("ScreenTargetCritical").GetComponent<Text>();
        PriorityImage = OnScreen.transform.FindChild("ScreenTargetPriority").GetComponent<Image>();
    }

    public void SetupEntityDisplays() {
        if (Self.isBuildZone && (Self.behaviour as BuildZoneObject).buildZone) {
            MPTeam team = (Self.behaviour as BuildZoneObject).buildZone.player.team;
            if (team == MPTeam.None) {
                Alignment = EntityAlignment.Neutral;
            }
            else {
                Alignment = (team == PlayerMachine.GetLocal().InternalObjectServer.player.team)
                    ? EntityAlignment.Ally : EntityAlignment.Enemy;
            }
            EntityName = (Self.behaviour as BuildZoneObject).buildZone.player.name;
        }

        TypeImage.sprite = TargetImage(Type, IsTarget);
        NameText.text = EntityName;
        TargetText.enabled = IsTarget;
        PriorityImage.enabled = IsPriority;

        MiniImage.sprite = MinimapImage(Type);
        MiniTargetImage.enabled = IsTarget;
        MiniPriorityImage.enabled = IsPriority;

        Color typeColor = AlignmentColor(Alignment, Type == EntityType.Hidden);

        TypeImage.color = typeColor;
        NameText.color = typeColor;
        DistanceText.color = typeColor;
        PriorityImage.color = typeColor;

        MiniImage.color = AlignmentColor(Alignment, Type == EntityType.Hidden, true);
        MiniTargetImage.color = (Type == EntityType.Hidden) ? Color.clear : Color.red;
    }

    // Update is called once per frame
    void Update() {
        if (!CameraHolder.CurrentCamera) return;

        if (!this.IsDead) {
            if (this.InFrontOfCamera) {
                OnScreen.SetActive(true);
                UpdateScreen();
            }
            else {
                OnScreen.SetActive(false);
            }

            OnMinimap.SetActive(true);
            UpdateMinimap();

            if (IsNextTarget && Type != EntityType.Hidden) {
                CurrentTarget = this;
            }
        }
        else {
            OnDisable();
        }
    }

    void UpdateScreen() {
        (OnScreen.transform as RectTransform).anchoredPosition = this.ScreenPosition;

        float distance = Vector3.Distance(CameraHolder.CurrentCamera.transform.position, SelfTransform.position);
        TypeImage.rectTransform.localScale = Vector3.one * ((distance > 500f) ? 500f / distance : 1f);
        PriorityImage.rectTransform.localScale = Vector3.one * ((distance > 500f) ? 500f / distance : 1f);

        DistanceText.enabled = (this == CurrentTarget);
        NameText.enabled = (this == CurrentTarget);
        TypeImage.sprite = TargetImage(Type, this == CurrentTarget);

        if (this == CurrentTarget) DistanceText.text = "" + Mathf.RoundToInt(distance);
    }

    void UpdateMinimap() {
        OnMinimap.transform.localPosition = new Vector3(
            (SelfTransform.position.x - CameraHolder.CurrentCamera.transform.position.x),
            (SelfTransform.position.z - CameraHolder.CurrentCamera.transform.position.z),
            0f) * MinimapDisplay.Scale;
        OnMinimap.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, SelfTransform.eulerAngles.y));
    }

    void OnDisable() {
        if (this == CurrentTarget) CurrentTarget = null;
        OnScreen.SetActive(false);
        if (OnMinimap.activeSelf) CameraHUDController.Destroyed.DisplayEntity(this);
        OnMinimap.SetActive(false);
    }

    public static Color AlignmentColor(EntityAlignment alignment, bool isHidden = false, bool isMinimap = false) {
        if (isHidden) return Color.clear;
        switch (alignment) {
            case EntityAlignment.Enemy:
                return (isMinimap) ? Color.white : Color.green;
            case EntityAlignment.Ally:
                return Color.cyan;
            case EntityAlignment.Neutral:
                return Color.yellow;
            default:
                return Color.green;
        }
    }
}
