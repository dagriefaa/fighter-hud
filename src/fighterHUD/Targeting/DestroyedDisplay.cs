﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Modding;

/*
 * List of last three objects destroyed (player or not).
 */
namespace FighterHUD;
public class DestroyedDisplay : MonoBehaviour {

    int FreezeCounter = 0;

    // Use this for initialization
    void Start() {
        Events.OnSimulationToggle += x => CleanUp();
    }

    void Update() {
        if (FreezeCounter > 0) FreezeCounter--;
    }

    public void CleanUp() {
        FreezeCounter = 2;
        for (int i = 0; i < transform.childCount; i++) {
            Destroy(transform.GetChild(i).gameObject);
        }
    }

    public void DisplayEntity(EntityDisplayController e) {
        if (FreezeCounter > 0) return;

        GameObject killed = Instantiate(CameraHUDController.Assets.LoadAsset<GameObject>("Assets/Prefabs/DestroyedTarget.prefab"));
        Text killText = killed.GetComponent<Text>();
        killText.text = "DESTROYED >> " + e.EntityName;
        killed.transform.SetParent(this.transform, false);
        killed.transform.SetAsFirstSibling();
        if (killed.transform.parent.childCount > 3) Destroy(killed.transform.parent.GetChild(3).gameObject);
        Destroy(killed, 5f);
    }
}
