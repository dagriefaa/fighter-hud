﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Security.Cryptography;

/*
 * Handles hinge settings.
 */
namespace FighterHUD;
public class HingeHolder : MonoBehaviour {

    SteeringWheel block;
    MMenu hingeMenu;

    void Awake() {
        block = GetComponent<SteeringWheel>();
        hingeMenu = new MMenu("hudHinges", 0, new List<string> { "HUD: None", "HUD: Landing\nGear", "HUD: Throttle ↻", "HUD: Throttle ↺" }, false);
        block.AddMenu(hingeMenu);
    }

    void Start() {
        if (!block.isSimulating || StatMaster.isMP && !StatMaster.isHosting && !StatMaster.isLocalSim) {
            return;
        }
        switch (hingeMenu.Value) {
            case 1: { MachineTracker.SetLandingGear(block); break; }
            case 2: { MachineTracker.SetThrottle(block, false); break; }
            case 3: { MachineTracker.SetThrottle(block, true); break; }
        }
    }
}
