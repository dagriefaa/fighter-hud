﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

/**
 * Landing gear angle display.
 */
namespace FighterHUD;
public class LandingGearDisplay : MonoBehaviour {

    UnityEngine.UI.Slider landingGearSlider;
    List<MaskableGraphic> graphics;

    void Awake() {
        landingGearSlider = GetComponent<UnityEngine.UI.Slider>();
        graphics = GetComponentsInChildren<MaskableGraphic>().ToList();
    }

    void OnEnable() {
        if (Machine.Active() && !MachineTracker.HasLandingGear) {
            graphics.ForEach(x => x.enabled = false);
        }
    }

    void Update() {
        if (MachineTracker.HasLandingGear) {
            landingGearSlider.value = MachineTracker.LandingGearRetractLevel;
            graphics.ForEach(x => x.enabled = landingGearSlider.value < 1);
        }
    }

}
