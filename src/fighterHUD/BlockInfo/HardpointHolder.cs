﻿using Modding.Blocks;
using System.Collections;
using UnityEngine;

/*
 * Handles hardpoint settings and graphics.
 */
namespace FighterHUD;
public class HardpointHolder : MonoBehaviour {

    public BlockBehaviour Block { get; private set; }


    bool jointFound = false;
    int hardpointId;
    ConfigurableJoint joint;
    MMenu weaponsMenu;

    void Awake() {
        Block = GetComponent<BlockBehaviour>();
        weaponsMenu = new MMenu("hudWeapons", 0, new() { "HUD: None", "HUD: Missile", "HUD: Bomb", "HUD: Other" }, false);
        Block.AddMenu(weaponsMenu);
    }

    IEnumerator Start() {
        if (!Block.isSimulating) {
            yield break;
        }

        for (int i = 0; i < 6; i++) {
            yield return new WaitForEndOfFrame();
        }

        if (weaponsMenu.Value == 0 || (Block.isSimulating && StatMaster.isClient && !StatMaster.isLocalSim)) {
            DestroyImmediate(this);
            yield break;
        }
        if (Block.isSimulating) {
            joint = Block switch {
                GrabberBlock grabber => grabber.joinOnTriggerBlock.currentJoint,
                ExplosiveBolt decoupler => decoupler.GetComponent<ConfigurableJoint>(),
                _ => null
            };
            if (!joint) {
                DestroyImmediate(this);
                yield break;
            }
            hardpointId = HardpointController.AddHardpoint(PlayerMachine.From(Block.ParentMachine), (HardpointController.WeaponType)weaponsMenu.Value, this.transform.localPosition);
            jointFound = true;
        }
    }

    void Update() {
        if (!Block.isSimulating || !jointFound) { return; }

        if (!joint) {
            HardpointController.TriggerHardpoint(PlayerMachine.From(Block.ParentMachine), hardpointId);
            Destroy(this);
        }
    }
}
