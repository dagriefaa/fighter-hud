﻿using UnityEngine;

/**
 * Value controller for camera blocks.
 */
namespace FighterHUD;
public class CameraHolder : MonoBehaviour {

    public static CameraHolder CurrentCamera = null;

    public FixedCameraBlock CameraBlock = null;

    public bool ShowHUD = false;
    public bool ShowAttitudeDisplay = true;

    public MKey Engine = null;
    public MKey ThrottleForward = null;
    public MKey ThrottleReverse = null;

    public bool IsEngineRunning = false;

    void Awake() {
        CameraBlock = GetComponent<FixedCameraBlock>();
        if (CameraBlock.isSimulating && !ShowHUD) {
            Destroy(this);
            return;
        }

        MToggle hudToggle = CameraBlock.AddToggle(new MToggle("HUD: Enable", "fighterHud", false));

        MToggle hudAttitude = CameraBlock.AddToggle(new MToggle("HUD: First Person\nDisplay", "hudFirstPerson", true));
        hudAttitude.DisplayInMapper = false;

        Engine = CameraBlock.AddKey(new MKey("HUD: Engine", "engineForward", KeyCode.LeftControl));
        Engine.DisplayInMapper = false;

        ThrottleForward = CameraBlock.AddKey(new MKey("HUD: Throttle +", "throttleForward", KeyCode.X));
        ThrottleForward.DisplayInMapper = false;

        ThrottleReverse = CameraBlock.AddKey(new MKey("HUD: Throttle -", "throttleReverse", KeyCode.Z));
        ThrottleReverse.DisplayInMapper = false;

        hudToggle.Toggled += state => {
            ShowHUD = state;
            hudAttitude.DisplayInMapper = state;
            Engine.DisplayInMapper = state;
            ThrottleForward.DisplayInMapper = state;
            ThrottleReverse.DisplayInMapper = state;
        };

        hudAttitude.Toggled += state => ShowAttitudeDisplay = state;

        if (StatMaster.isMP && !StatMaster.isClient && CameraBlock.ParentMachine && !CameraBlock.ParentMachine.isLocalMachine) {
            CameraBlock.enabled = false; // workaround to get camera on client machine to attach
        }
    }

    void Update() {
        if (!CameraBlock.isSimulating) {
            return;
        }

        if (Engine.IsReleased || Engine.EmulationReleased()) {
            IsEngineRunning = !IsEngineRunning;
        }

        if (FixedCameraController.Instance.activeCamera == null && CurrentCamera != null) {
            CurrentCamera = null;
            MachineTracker.SetCurrentCamera(null);
        }
        else if (FixedCameraController.Instance.activeCamera == CameraBlock && CurrentCamera != this) {
            CurrentCamera = this;
            MachineTracker.SetCurrentCamera(CameraBlock);
        }
    }

}
