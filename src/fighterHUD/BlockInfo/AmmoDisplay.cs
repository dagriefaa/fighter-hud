﻿using Modding;
using Modding.Blocks;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

/*
 * Crossbow ammunition counter.
 */
namespace FighterHUD;
public class AmmoDisplay : MonoBehaviour {

    Text crossbowText;
    Text cannonText;

    List<CrossBowBlock> crossbows;
    List<CanonBlock> cannons;

    void Start() {
        crossbowText = transform.Find("Crossbows").GetComponent<Text>();
        cannonText = transform.Find("Cannons").GetComponent<Text>();
        Events.OnMachineSimulationToggle += OnMachineSimulationToggle;
        OnMachineSimulationToggle(PlayerMachine.GetLocal(), true);
    }

    void OnMachineSimulationToggle(PlayerMachine machine, bool sim) {
        if (sim && machine == PlayerMachine.GetLocal()) {
            crossbows = machine.GetBlocksOfType(BlockType.Crossbow).Select(b => (CrossBowBlock)b.InternalObject).ToList();
            cannons = machine.GetBlocksOfType(BlockType.Cannon).Select(b => (CanonBlock)b.InternalObject).ToList();

            crossbowText.gameObject.SetActive(crossbows.Count > 0);
            cannonText.gameObject.SetActive(cannons.Count > 0);
        }
    }

    void Update() {
        if (PlayerMachine.GetLocal().InternalObject.InfiniteAmmoMode) {
            crossbowText.text = "---";
            cannonText.text = "---";
            return;
        }
        if (crossbowText.isActiveAndEnabled) {
            int crossbowBolts = 0;
            foreach (CrossBowBlock crossbow in crossbows) {
                crossbowBolts += crossbow.ammo;
            }
            crossbowText.text = crossbowBolts.ToString();
        }

        if (cannonText.isActiveAndEnabled) {
            int cannonShells = 0;
            foreach (CanonBlock cannon in cannons) {
                cannonShells += cannon.Ammo;
            }
            cannonText.text = cannonShells.ToString();
        }
    }
}
