﻿using Modding;
using Modding.Blocks;
using Modding.Common;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace FighterHUD;
public class HardpointController : MonoBehaviour {

    public static MessageType MAddHardpoint;
    public static MessageType MTriggerHardpoint;

    public static void SetupMessages() {
        MAddHardpoint = ModNetworking.CreateMessageType(DataType.Machine, DataType.Integer, DataType.Vector3);
        ModNetworking.Callbacks[MAddHardpoint] += msg => {
            var m = (PlayerMachine)msg.GetData(0);
            var type = (WeaponType)msg.GetData(1);
            var position = (Vector3)msg.GetData(2);
            AddHardpointVis(m, type, position);
        };
        MTriggerHardpoint = ModNetworking.CreateMessageType(DataType.Machine, DataType.Integer);
        ModNetworking.Callbacks[MTriggerHardpoint] += msg => {
            var m = (PlayerMachine)msg.GetData(0);
            var hardpointId = (int)msg.GetData(1);
            TriggerHardpointVis(m, hardpointId);
        };
    }

    public enum WeaponType {
        None, Missile, Bomb, Other
    }

    class Hardpoint {
        public Image Image;
        public Vector3 Position;
        public bool IsTriggered = false;
        public WeaponType Type;

        public Hardpoint(Image image, WeaponType type, Vector3 position) {
            Image = image;
            Position = position;
            Type = type;
        }
    }

    static Dictionary<ushort, List<Hardpoint>> MachineHardpoints = new();

    static Sprite missileReady;
    static Sprite missileSpent;
    static Sprite bombReady;
    static Sprite bombSpent;
    static Sprite otherReady;
    static Sprite otherSpent;

    public Text missileCount;
    public Text bombCount;
    public Text otherCount;
    static Sprite WeaponImage(WeaponType type, bool spent) => type switch {
        WeaponType.Missile => spent ? missileSpent : missileReady,
        WeaponType.Bomb => spent ? bombSpent : bombReady,
        WeaponType.Other => spent ? otherSpent : otherReady,
        _ => throw new NotImplementedException()
    };

    void Start() {
        Events.OnMachineSimulationToggle += OnSimulationToggle;
        Events.OnActiveSceneChanged += OnSceneChanged;

        missileReady = ModResource.GetTexture("WeaponMissile.Ready").ToSprite();
        missileSpent = ModResource.GetTexture("WeaponMissile.Spent").ToSprite();
        bombReady = ModResource.GetTexture("WeaponBomb.Ready").ToSprite();
        bombSpent = ModResource.GetTexture("WeaponBomb.Spent").ToSprite();
        otherReady = ModResource.GetTexture("WeaponOther.Ready").ToSprite();
        otherSpent = ModResource.GetTexture("WeaponOther.Spent").ToSprite();
    }

    void OnSimulationToggle(PlayerMachine m, bool sim) {
        if (!sim) {
            return;
        }
        if (!MachineHardpoints.TryGetValue(m.InternalObject.PlayerID, out var hardpoints)) {
            hardpoints = new();
            MachineHardpoints.Add(m.InternalObject.PlayerID, hardpoints);
        }
        if (m == PlayerMachine.GetLocal()) {
            hardpoints.ForEach(h => Destroy(h.Image.gameObject));
            StartCoroutine(UpdateHardpointOrder(hardpoints));
        }
        hardpoints.Clear();
    }

    void OnSceneChanged(Scene scene1, Scene scene2) {
        foreach (var hardpoints in MachineHardpoints.Values) {
            hardpoints.FindAll(h => h.Image).ForEach(h => Destroy(h.Image.gameObject));
            hardpoints.Clear();
        }
        MachineHardpoints.Clear();
    }

    void Update() {
        if (Mod.SceneNotPlayable()) {
            return;
        }
        if (Machine.Active() && Machine.Active().isSimulating) {
            var hardpoints = MachineHardpoints[Machine.Active().PlayerID];

            // weapon counting
            var missiles = 0;
            var bombs = 0;
            var others = 0;
            missileCount.gameObject.SetActive(false);
            bombCount.gameObject.SetActive(false);
            otherCount.gameObject.SetActive(false);
            foreach (var hardpoint in hardpoints) {
                switch (hardpoint.Type) {
                    case WeaponType.Missile: {
                        missileCount.gameObject.SetActive(true);
                        if (!hardpoint.IsTriggered) {
                            missiles++;
                        }
                        break;
                    }
                    case WeaponType.Bomb: {
                        bombCount.gameObject.SetActive(true);
                        if (!hardpoint.IsTriggered) {
                            bombs++;
                        }
                        break;
                    }
                    case WeaponType.Other: {
                        otherCount.gameObject.SetActive(true);
                        if (!hardpoint.IsTriggered) {
                            others++;
                        }
                        break;
                    }
                }
            }
            missileCount.text = missiles.ToString();
            bombCount.text = bombs.ToString();
            otherCount.text = others.ToString();

            // health coloring
            if (StatMaster.isMP) {
                var color = HealthColourController.HealthColor(PlayerMachine.GetLocal().Health);
                foreach (var hardpoint in hardpoints) {
                    hardpoint.Image.color = color;
                }
            }
        }
    }

    IEnumerator UpdateHardpointOrder(List<Hardpoint> hardpoints) {
        yield return new WaitForEndOfFrame();
        yield return new WaitUntil(() => hardpoints.Count > 0);

        hardpoints = hardpoints
            .OrderBy(x => x.Position.x)
            .ThenBy(x => x.Position.z)
            .ToList();

        // slice hardpoints into two halves
        var left = hardpoints.Take(hardpoints.Count / 2).Reverse().ToList();
        var right = hardpoints.Skip(hardpoints.Count / 2).ToList();

        // the images for each half slope away from the center
        for (int i = 0; i < left.Count(); i++) {
            (left[i].Image.transform as RectTransform).localPosition = new Vector3(-50 - (25 * i), -12.5f * i);
        }
        for (int i = 0; i < right.Count(); i++) {
            (right[i].Image.transform as RectTransform).localPosition = new Vector3(50 + (25 * i), -12.5f * i);
        }
    }

    public static int AddHardpoint(PlayerMachine m, WeaponType type, Vector3 position) {
        if (StatMaster.isMP && StatMaster.isClient && !StatMaster.isLocalSim) {
            throw new InvalidOperationException("Client called method");
        }
        if (StatMaster.isMP) {
            ModNetworking.SendTo(Player.From(m.InternalObject.PlayerID), MAddHardpoint.CreateMessage(m, (int)type, position));
        }
        return AddHardpointVis(m, type, position);
    }

    public static void TriggerHardpoint(PlayerMachine m, int hardpointId) {
        if (StatMaster.isMP && StatMaster.isClient && !StatMaster.isLocalSim) {
            throw new InvalidOperationException("Client called method");
        }
        if (StatMaster.isMP) {
            ModNetworking.SendTo(Player.From(m.InternalObject.PlayerID), MTriggerHardpoint.CreateMessage(m, hardpointId));
        }
        TriggerHardpointVis(m, hardpointId);
    }

    static int AddHardpointVis(PlayerMachine m, WeaponType type, Vector3 position) {
        if (!MachineHardpoints.TryGetValue(m.InternalObject.PlayerID, out var hardpoints)) {
            hardpoints = new();
            MachineHardpoints.Add(m.InternalObject.PlayerID, hardpoints);
        }

        Image image = null;
        if (m == PlayerMachine.GetLocal()) {
            var wep = Instantiate(CameraHUDController.WeaponPrefab);
            wep.SetActive(true);
            wep.transform.SetParent(CameraHUDController.WeaponsPlane.transform, false);
            image = wep.GetComponent<Image>();
            image.sprite = WeaponImage(type, false);
        }

        hardpoints.Add(new(image, type, position));
        return hardpoints.Count - 1;
    }

    static void TriggerHardpointVis(PlayerMachine m, int hardpointId) {
        if (!MachineHardpoints.TryGetValue(m.InternalObject.PlayerID, out var hardpoints)) {
            throw new InvalidOperationException($"Hardpoints for machine {m.InternalObject.PlayerID} not initialised.");
        }
        var hardpoint = hardpoints[hardpointId];
        if (hardpoint.IsTriggered) {
            return;
        }

        if (m == PlayerMachine.GetLocal()) {
            hardpoint.Image.sprite = WeaponImage(hardpoint.Type, true);
        }

        hardpoint.IsTriggered = true;
    }
}
