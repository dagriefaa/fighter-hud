﻿using Modding;
using Modding.Blocks;
using UnityEngine;
using UnityEngine.UI;

/*
 * Controls when the screen ices up.
 */
namespace FighterHUD;
public class IcingController : MonoBehaviour {

    IceController ice;
    Image icing;
    GameObject iceWarning;
    GameObject caution;

    bool isFrozen = false;

    public static bool IcingEnabled = true;

    // Use this for initialization
    void Awake() {
        icing = GetComponent<Image>();
        iceWarning = CameraHUDController.Screen.FindChild("Ice Warning 2").gameObject;
        caution = CameraHUDController.Screen.FindChild("Ice Warning 1").gameObject;
        Events.OnMachineSimulationToggle += OnSimulationStart;
    }

    void OnSimulationStart(PlayerMachine _, bool entering) {
        if (!entering) return;
        this.enabled = true;
        isFrozen = false;
        icing.color = Color.clear;
        caution.SetActive(false);
        iceWarning.SetActive(false);
    }

    void Update() {
        if (!ice) {
            ice = Resources.FindObjectsOfTypeAll<IceController>()[0];
            if (!ice) {
                this.enabled = false;
                return;
            }
        }

        if (!IcingEnabled || !CameraHolder.CurrentCamera) return;

        if (CameraHolder.CurrentCamera.transform.position.y > ice.transform.position.y) {
            isFrozen = true;
            iceWarning.SetActive(true);
        }

        if (isFrozen) {
            if (icing.color != Color.white) icing.color = Color.Lerp(icing.color, Color.white, Time.deltaTime);
            caution.SetActive((Time.time % 1) < 0.5f);
        }
    }
}
