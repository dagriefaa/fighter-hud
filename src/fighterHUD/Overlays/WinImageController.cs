﻿using Modding;
using Modding.Blocks;
using Modding.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FighterHUD;
public class WinImageController : MonoBehaviour {

    AudioSource audio;
    GameObject victory;
    GameObject failure;

    WinScreen winScreen;
    AudioSource winAudio;
    AudioSource stampAudio;
    AudioSource triumphAudio;
    float winVolume;
    float stampVolume;
    float bigStampVolume;
    float triumphVolume;

    void Awake() {
        audio = GetComponent<AudioSource>();
        victory = transform.FindChild("Victory").gameObject;
        failure = transform.FindChild("Failure").gameObject;
        Events.OnLevelWonMP += OnLevelWonMP;
        ReferenceMaster.onLevelWon += OnLevelWon;
        Events.OnActiveSceneChanged += OnActiveSceneChanged;
        Events.OnSimulationToggle += OnSimulationToggle;
        OnActiveSceneChanged(default, default);
    }

    void OnSimulationToggle(bool sim) {
        if (!sim) {
            audio.Stop();
            victory.SetActive(false);
            failure.SetActive(false);
        }
    }

    void OnActiveSceneChanged(UnityEngine.SceneManagement.Scene arg1, UnityEngine.SceneManagement.Scene arg2) {
        if (Mod.SceneNotPlayable()) {
            return;
        }

        winScreen = GameObject.FindObjectOfType<WinScreen>();
        winAudio = winScreen.GetComponent<AudioSource>();
        stampAudio = winScreen.stamp.stampNormal.GetComponent<AudioSource>();
        triumphAudio = winScreen.triumphBarLerpIn.GetComponent<AudioSource>();
        winVolume = winAudio.volume;
        stampVolume = stampAudio.volume;
        bigStampVolume = winScreen.stamp.stampBig.stampAudio.volume;
        triumphVolume = triumphAudio.volume;
    }

    void OnLevelWon() {
        if (StatMaster.isMP) {
            return;
        }
        StopAllCoroutines();
        StartCoroutine(ShowImage(true));
    }

    void OnLevelWonMP(List<MPTeam> list) {
        StopAllCoroutines();
        StartCoroutine(ShowImage(list.Contains(Player.GetLocalPlayer().Team)));
    }

    void OnEnable() {
        winScreen.stamp.gameObject.SetActive(false);
        winScreen.triumphBarLerpIn.gameObject.SetActive(false);
        winAudio.volume = 0;
        stampAudio.volume = 0;
        winScreen.stamp.stampBig.stampAudio.volume = 0;
        triumphAudio.volume = 0;
    }

    void OnDisable() {
        winScreen.stamp.gameObject.SetActive(true);
        winScreen.triumphBarLerpIn.gameObject.SetActive(true);
        winAudio.volume = winVolume;
        stampAudio.volume = stampVolume;
        winScreen.stamp.stampBig.stampAudio.volume = bigStampVolume;
        triumphAudio.volume = triumphVolume;
    }

    IEnumerator ShowImage(bool won) {
        GameObject image = won ? victory : failure;
        audio.pitch = won ? 1 : 0.75f;
        audio.Play();
        image.SetActive(true);
        yield return new WaitForSecondsRealtime(5);
        image.SetActive(false);
    }
}
