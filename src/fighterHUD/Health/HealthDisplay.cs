﻿using Modding.Blocks;
using UnityEngine;
using UnityEngine.UI;

/*
 * Health counter.
 */
namespace FighterHUD;
public class HealthDisplay : MonoBehaviour {

    Text healthText;

    void Start() {
        healthText = transform.Find("Health").GetComponent<Text>();
    }

    void Update() {
        var enabled = StatMaster.isMP && (PlayerMachine.GetLocal()?.InternalObjectServer.registerDamage ?? false);
        healthText.gameObject.SetActive(enabled);
        if (enabled) {
            healthText.text = $"{Mathf.Round((1f - PlayerMachine.GetLocal().Health) * 100)}%";
        }
    }
}
