﻿using Modding.Common;
using UnityEngine;
using UnityEngine.UI;

namespace FighterHUD.Health;
public class UsernameDisplay : MonoBehaviour {

    Text text;

    void Start() {
        text = transform.Find("Username").GetComponent<Text>();
    }

    void Update() {
        text.gameObject.SetActive(StatMaster.isMP);
        if (StatMaster.isMP) {
            text.text = Player.GetLocalPlayer().Name;
        }
    }
}
