﻿using Modding.Blocks;
using UnityEngine;
using UnityEngine.UI;

/*
 * Controls colour of certain objects with player health.
 */
namespace FighterHUD;
public class HealthColourController : MonoBehaviour {

    MaskableGraphic[] graphics;

    void Start() {
        graphics = GetComponentsInChildren<MaskableGraphic>();
    }

    void Update() {
        Color healthColor = HealthColor(PlayerMachine.GetLocal().Health);
        foreach (MaskableGraphic graphic in graphics) {
            if (!graphic) {
                continue;
            }
            graphic.color = new Color(healthColor.r, healthColor.g, healthColor.b, graphic.color.a);
        }
    }

    public static Color HealthColor(float health) {
        health -= 0.5f;
        if (!(Machine.Active() is ServerMachine s && s.registerDamage)) {
            return Color.green;
        }
        return (health > 0f) ? Color.HSVToRGB(health * (2f / 3f), 1, 1) : Color.red;
    }
}
