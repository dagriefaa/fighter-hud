using Modding;
using System;
using UnityEngine;

namespace FighterHUD;
public class Mod : ModEntryPoint {

    public static GameObject ModControllerObject;

    public override void OnLoad() {

        ModControllerObject = GameObject.Find("ModControllerObject");
        if (!ModControllerObject) {
            UnityEngine.Object.DontDestroyOnLoad(ModControllerObject = new GameObject("ModControllerObject"));
        }

        if (ModResource.AllResourcesLoaded) { ModControllerObject.AddComponent<CameraHUDController>(); }
        else { ModResource.OnAllResourcesLoaded += () => ModControllerObject.AddComponent<CameraHUDController>(); }

        if (Mods.IsModLoaded(new Guid("3c1fa3de-ec74-44e4-807c-9eced79ddd3f"))) {
            ModControllerObject.AddComponent<Mapper>();
        }

        // multiplayer messages
        MachineTracker.SetupMessages();
        HardpointController.SetupMessages();
    }

    public static bool SceneNotPlayable() {
        return !AdvancedBlockEditor.Instance;
    }

}
