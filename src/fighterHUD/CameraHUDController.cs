﻿using FighterHUD.Health;
using Modding;
using Modding.Blocks;
using Modding.Levels;
using System;
using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/**
 * Central controller.
 * 
 * - add rain overlay
 */
namespace FighterHUD;
public class CameraHUDController : MonoBehaviour {

    bool _disableHUDTimer = false;
    public bool DisableHUDTimer {
        get {
            return _disableHUDTimer;
        }
        set {
            if (_disableHUDTimer == value && !startup) return;
            _disableHUDTimer = value;
            Modding.Configuration.GetData().Write("DisableHUDTimer", value);
            Events.OnActiveSceneChanged -= HideTimer;
            if (value) {
                Events.OnActiveSceneChanged += HideTimer;
            }
            HideTimer(SceneManager.GetActiveScene(), SceneManager.GetActiveScene());
        }
    }
    bool _disableMachineIcon = false;
    public bool DisableMachineIcon {
        get {
            return _disableMachineIcon;
        }
        set {
            if (_disableMachineIcon == value && !startup) return;
            _disableMachineIcon = value;
            Modding.Configuration.GetData().Write("DisableMachineIcon", value);
            Events.OnActiveSceneChanged -= HideMachineIcon;
            if (value) {
                Events.OnActiveSceneChanged += HideMachineIcon;
            }
            HideMachineIcon(SceneManager.GetActiveScene(), SceneManager.GetActiveScene());
        }
    }
    bool _disableIce = false;
    public bool DisableIceOverlay {
        get {
            return _disableIce;
        }
        set {
            if (_disableIce == value && !startup) return;
            _disableIce = value;
            Modding.Configuration.GetData().Write("DisableIceOverlay", value);
            Events.OnActiveSceneChanged -= HideIce;
            if (value) {
                Events.OnActiveSceneChanged += HideIce;
            }
            HideIce(SceneManager.GetActiveScene(), SceneManager.GetActiveScene());
        }
    }

    public static AssetBundle Assets { get; private set; }
    public static GameObject WeaponPrefab { get; private set; }
    public static GameObject EntityTargetPrefab { get; private set; }
    public static GameObject EntityMinimapPrefab { get; private set; }

    public static Transform Screen { get; private set; }
    public static Canvas ScreenCanvas { get; private set; }
    public static Transform Minimap { get; private set; }
    public static TargetPointerController TargetPointer { get; private set; }

    public static Transform PointerFrame { get; private set; }

    public static TargetNameDisplay TargetName { get; private set; }
    public static DestroyedDisplay Destroyed { get; private set; }
    public static SimulationTimeDisplay SimulationTime { get; private set; }
    public static SpeedDisplay Speed { get; private set; }
    public static AltitudeDisplay Altitude { get; private set; }
    public static HeadingDisplay Heading { get; private set; }
    public static VelocityDisplay Velocity { get; private set; }
    public static EngineDisplay Engine { get; private set; }
    public static AttitudeDisplay Attitude { get; private set; }

    public static MainProgressDisplay MainProgress { get; private set; }
    public static MultiProgressDisplay NoneProgress { get; private set; }
    public static MultiProgressDisplay RedProgress { get; private set; }
    public static MultiProgressDisplay BlueProgress { get; private set; }
    public static MultiProgressDisplay GreenProgress { get; private set; }
    public static MultiProgressDisplay YellowProgress { get; private set; }

    public static HealthColourController HealthColour { get; private set; }
    public static UsernameDisplay UsernameText { get; private set; }
    public static HealthDisplay HealthCounter { get; private set; }
    public static AmmoDisplay AmmoCounter { get; private set; }

    public static LandingGearDisplay LandingGear { get; private set; }
    public static GameObject WeaponsPlane { get; private set; }
    public static HardpointController HardpointOrder { get; private set; }

    public static GameObject GodAmmo { get; private set; }
    public static GameObject GodInvincible { get; private set; }
    public static GameObject GodExplosive { get; private set; }
    public static GameObject GodFire { get; private set; }
    public static GameObject GodHand { get; private set; }
    public static GameObject GodZeroG { get; private set; }

    public static IcingController Ice { get; private set; }
    public static WinImageController Victory { get; private set; }

    public static CameraHUDController Instance { get; private set; }

    public static Camera MainCamera { get; private set; } = null;

    public static event Action OnTextUpdate;

    static Camera HUDCamera;

    static ModKey TargetButton = null;
    public static bool GetNextTarget { get { return TargetButton.IsPressed; } }

    public static System.Diagnostics.Stopwatch SimTimer { get; private set; } = new();

    static bool enteringSimulation = false;

    bool startup = true;

    void Start() {
        Assets = ModResource.GetAssetBundle("hud");
        WeaponPrefab = Assets.LoadAsset<GameObject>("Assets/Prefabs/Weapon.prefab");
        EntityMinimapPrefab = Assets.LoadAsset<GameObject>("Assets/Prefabs/MiniTarget.prefab");
        EntityTargetPrefab = Assets.LoadAsset<GameObject>("Assets/Prefabs/ScreenTarget.prefab");
        WeaponPrefab.transform.SetParent(this.transform, false);
        EntityMinimapPrefab.transform.SetParent(this.transform, false);
        EntityTargetPrefab.transform.SetParent(this.transform, false);
        WeaponPrefab.SetActive(false);
        EntityMinimapPrefab.SetActive(false);

        EntityTargetPrefab.SetActive(false);

        TargetButton = ModKeys.GetKey("target-key");

        gameObject.AddComponent<MachineTracker>();

        Events.OnSimulationToggle += OnSimulationToggle;
        Events.OnBlockInit += AddControllers;
        ReferenceMaster.onLevelWon += SimTimer.Stop;

        EntityDisplayController.SetupResources();

        GameObject screenObject = Instantiate(Assets.LoadAsset<GameObject>("Assets/HUD Canvas.prefab"),
            FighterHUD.Mod.ModControllerObject.transform, false) as GameObject;
        Screen = screenObject.transform;
        ScreenCanvas = Screen.GetComponent<Canvas>();
        ScreenCanvas.renderMode = RenderMode.ScreenSpaceCamera;
        ScreenCanvas.gameObject.layer = 7; // inbuilt layer

        Minimap = Screen.FindChild("Minimap/Minimap Frame");
        Minimap.gameObject.AddComponent<MinimapDisplay>();
        TargetPointer = ScreenObject("Target Pointer").AddComponent<TargetPointerController>();

        PointerFrame = Screen.FindChild("First Person Frame");

        SimulationTime = ScreenObject("Progress Frame/Sim Time Text").AddComponent<SimulationTimeDisplay>();
        SimulationTime.Start();
        TargetName = ScreenObject("Progress Frame/Target Text").AddComponent<TargetNameDisplay>();
        Destroyed = ScreenObject("Progress Frame/Destroyed Frame").AddComponent<DestroyedDisplay>();

        Speed = ScreenObject("Speed").AddComponent<SpeedDisplay>();
        Altitude = ScreenObject("Altitude").AddComponent<AltitudeDisplay>();
        Heading = ScreenObject("First Person Frame/Heading Frame").AddComponent<HeadingDisplay>();
        Velocity = ScreenObject("Velocity Vector").AddComponent<VelocityDisplay>();

        Engine = ScreenObject("First Person Frame").AddComponent<EngineDisplay>();
        Attitude = ScreenObject("First Person Frame/Attitude Mask/Attitude").AddComponent<AttitudeDisplay>();

        MainProgress = ScreenObject("Progress Frame/Progress Slider Main").AddComponent<MainProgressDisplay>();
        MainProgress.label = ScreenObject("Progress Frame/Progress Label");
        NoneProgress = ScreenObject("Progress Frame/Progress Bar Frame/Progress Slider None").AddComponent<MultiProgressDisplay>();
        RedProgress = ScreenObject("Progress Frame/Progress Bar Frame/Progress Slider Red").AddComponent<MultiProgressDisplay>();
        RedProgress.team = MPTeam.Red;
        BlueProgress = ScreenObject("Progress Frame/Progress Bar Frame/Progress Slider Blue").AddComponent<MultiProgressDisplay>();
        BlueProgress.team = MPTeam.Blue;
        GreenProgress = ScreenObject("Progress Frame/Progress Bar Frame/Progress Slider Green").AddComponent<MultiProgressDisplay>();
        GreenProgress.team = MPTeam.Green;
        YellowProgress = ScreenObject("Progress Frame/Progress Bar Frame/Progress Slider Yellow").AddComponent<MultiProgressDisplay>();
        YellowProgress.team = MPTeam.Orange;

        HealthColour = ScreenObject("Weapons Frame").AddComponent<HealthColourController>();
        UsernameText = ScreenObject("Weapons Frame/Weapon Counts").AddComponent<UsernameDisplay>();
        HealthCounter = ScreenObject("Weapons Frame/Weapon Counts").AddComponent<HealthDisplay>();
        AmmoCounter = ScreenObject("Weapons Frame/Weapon Counts").AddComponent<AmmoDisplay>();

        HardpointOrder = this.gameObject.AddComponent<HardpointController>();
        HardpointOrder.missileCount = ScreenObject("Weapons Frame/Weapon Counts/Missiles").GetComponent<Text>();
        HardpointOrder.bombCount = ScreenObject("Weapons Frame/Weapon Counts/Bombs").GetComponent<Text>();
        HardpointOrder.otherCount = ScreenObject("Weapons Frame/Weapon Counts/Others").GetComponent<Text>();

        WeaponsPlane = ScreenObject("Weapons Frame/Weapons Craft");
        LandingGear = ScreenObject("Weapons Frame/Landing Gear Slider").AddComponent<LandingGearDisplay>();

        GodAmmo = ScreenObject("Weapons Frame/Cheats/Ammo");
        GodInvincible = ScreenObject("Weapons Frame/Cheats/Invincible");
        GodExplosive = ScreenObject("Weapons Frame/Cheats/Explosive");
        GodFire = ScreenObject("Weapons Frame/Cheats/Fire");
        GodHand = ScreenObject("Weapons Frame/Cheats/Hand");
        GodZeroG = ScreenObject("Weapons Frame/Cheats/ZeroG");

        Ice = ScreenObject("Ice").AddComponent<IcingController>();
        Ice.gameObject.SetActive(true);

        ScreenCanvas.gameObject.SetActive(false);

        Victory = ScreenObject("Mission End").AddComponent<WinImageController>();

        Instance = this;

        DisableHUDTimer = (!Configuration.GetData().HasKey("DisableHUDTimer")) ? false : Configuration.GetData().ReadBool("DisableHUDTimer");
        DisableMachineIcon = (!Configuration.GetData().HasKey("DisableMachineIcon")) ? false : Configuration.GetData().ReadBool("DisableMachineIcon");
        DisableIceOverlay = (!Configuration.GetData().HasKey("DisableIceOverlay")) ? false : Configuration.GetData().ReadBool("DisableIceOverlay");


        StartCoroutine(IEUpdateText());
    }

    GameObject ScreenObject(string path) {
        var result = Screen.FindChild(path);
        if (!result) {
            Debug.LogError($"Screen/{path} not found", this);
        }
        return result?.gameObject;
    }

    void OnSimulationToggle(bool sim) {
        enteringSimulation = sim;
        if (sim) {
            SimTimer.Reset();
            SimTimer.Start();
        }
        else {
            EntityDisplayController.Entities.ForEach(x => Destroy(x));
            SimTimer.Stop();
        }
    }

    private void AddControllers(Block block) {
        if (block.InternalObject.isSimulating) {
            return;
        }
        if (block.InternalObject is FixedCameraBlock) {
            block.InternalObject.gameObject.AddComponent<CameraHolder>();
        }

        if (block.InternalObject is SteeringWheel && block.InternalObject.Prefab.Type == BlockType.SteeringHinge) {
            block.InternalObject.gameObject.AddComponent<HingeHolder>();
        }

        if (block.InternalObject is ExplosiveBolt || block.InternalObject is GrabberBlock) {
            block.InternalObject.gameObject.AddComponent<HardpointHolder>();
        }
    }

    void Update() {
        if (FighterHUD.Mod.SceneNotPlayable()) return;
        if (!MainCamera) MainCamera = Camera.main;
        ScreenCanvas.gameObject.SetActive(CameraHolder.CurrentCamera);
        if (!ScreenCanvas.worldCamera) {
            if (!HUDCamera) {
                HUDCamera = (Instantiate(MainCamera.transform.FindChild("3D Hud Cam").gameObject, MainCamera.transform, true) as GameObject).GetComponent<Camera>();
                HUDCamera.cullingMask = (1 << 7);
                MainCamera.cullingMask &= ~HUDCamera.cullingMask;
                HUDCamera.clearFlags = CameraClearFlags.Depth;
                HUDCamera.gameObject.name = "Camera HUD Camera";
            }
            ScreenCanvas.worldCamera = HUDCamera;
            //Screen.transform.parent = MainCamera.transform;
        }
        if (HUDCamera) HUDCamera.fieldOfView = MainCamera.fieldOfView;

        if (CameraHolder.CurrentCamera) PointerFrame.gameObject.SetActive(CameraHolder.CurrentCamera.ShowAttitudeDisplay);

        if (enteringSimulation) {
            Level.GetCurrentLevel()?.Entities.ToList().ForEach(e => EntityDisplayController.From(e));
            Destroyed.CleanUp();
            enteringSimulation = false;
        }

        // update god modes
        GodAmmo.SetActive(StatMaster.GodTools.InfiniteAmmoMode);
        GodInvincible.SetActive(StatMaster.GodTools.UnbreakableMode);
        GodExplosive.SetActive(StatMaster.GodTools.ExplodingCannonballs);
        GodFire.SetActive(StatMaster.GodTools.PyroMode);
        GodHand.SetActive(StatMaster.GodTools.DragMode);
        GodZeroG.SetActive(StatMaster.GodTools.GravityDisabled);
    }

    IEnumerator IEUpdateText() {
        while (true) {
            if (CameraHolder.CurrentCamera) {
                OnTextUpdate?.Invoke();
            }
            yield return new WaitForSeconds(0.1f);
        }
    }

    void HideTimer(Scene a, Scene b) {
        SimulationTime.SimTimeText.enabled = !DisableHUDTimer;
    }

    void HideMachineIcon(Scene a, Scene b) {
        WeaponsPlane.GetComponent<Image>().enabled = !DisableMachineIcon;
    }

    void HideIce(Scene a, Scene b) {
        IcingController.IcingEnabled = !DisableIceOverlay;
    }


}
