﻿using Modding;
using System;
using UnityEngine;
using UnityEngine.UI;

/*
 * Controls the prograde/retrograde vector display.
 */
namespace FighterHUD;
public class VelocityDisplay : MonoBehaviour {

    Image velocityVector;

    Sprite prograde;
    Sprite retrograde;

    // Use this for initialization
    void Start() {
        velocityVector = GetComponent<Image>();

        prograde = ModResource.GetTexture("Prograde").ToSprite();
        retrograde = ModResource.GetTexture("Retrograde").ToSprite();
    }

    // Update is called once per frame
    void FixedUpdate() {
        if (!CameraHolder.CurrentCamera) return;

        Vector3 localVelocity = CameraHUDController.MainCamera.transform.InverseTransformDirection(MachineTracker.Velocity);
        Vector3 normalisedVelocity = localVelocity.normalized * 200f;
        float velocityX = Mathf.Clamp(normalisedVelocity.x, -200f, 200f);
        float velocityY = Mathf.Clamp(normalisedVelocity.y, -200f, 200f);

        velocityVector.rectTransform.localPosition = new Vector3(velocityX, velocityY, 0);
        velocityVector.sprite = localVelocity.z > 0 ? prograde : retrograde;
        velocityVector.color = new Color(0, 1, 0, Math.Abs(localVelocity.z) / 10);
    }
}
