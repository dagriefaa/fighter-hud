﻿using UnityEngine;
using UnityEngine.UI;

/**
 * Display controller for speed text and ruler.
 */
namespace FighterHUD;
public class SpeedDisplay : MonoBehaviour {

    public static float MaxHeight = 2000f;

    Ruler speedRuler;
    Text speedText;

    void Start() {
        speedText = GetComponent<Text>();
        speedRuler = CameraHUDController.Screen.FindChild("First Person Frame/Speed Ruler").gameObject.AddComponent<Ruler>();
        speedRuler.Mode = Ruler.Direction.Vertical;

        CameraHUDController.OnTextUpdate += () => speedText.text = Mathf.RoundToInt(MachineTracker.Velocity.magnitude * 3.6f).ToString();
    }

    void Update() {
        if (!CameraHolder.CurrentCamera) { return; }
        if (CameraHolder.CurrentCamera.ShowAttitudeDisplay) {
            speedRuler.Value = MachineTracker.Velocity.magnitude * 3.6f;
        }
    }

}
