﻿using UnityEngine;
using UnityEngine.UI;

/**
 * Display controller for heading text and ruler.
 */
namespace FighterHUD;
public class HeadingDisplay : MonoBehaviour {

    Text headingText;
    Ruler headingTape;

    Text compass90;
    Text compass45;

    void Start() {
        headingText = transform.FindChild("Heading Text").GetComponent<Text>();
        headingTape = transform.FindChild("Heading Tape").gameObject.AddComponent<Ruler>();
        headingTape.Mode = Ruler.Direction.Horizontal;

        compass90 = transform.Find("Heading Tape/Dir (1)").GetComponent<Text>();
        compass45 = transform.Find("Heading Tape/Dir (2)").GetComponent<Text>();

        CameraHUDController.OnTextUpdate += () => headingText.text = Mathf.RoundToInt(CameraHUDController.MainCamera.transform.rotation.eulerAngles.y).ToString();
    }

    void Update() {
        if (!CameraHolder.CurrentCamera) { return; }

        float y = CameraHUDController.MainCamera.transform.rotation.eulerAngles.y;
        headingTape.Value = y / 45f * 128f;

        compass90.enabled = Mathf.Abs(compass90.rectTransform.localPosition.x) > 40;
        compass45.enabled = Mathf.Abs(compass45.rectTransform.localPosition.x) > 40;

        compass90.text = y switch {
            >= 315 or < 45 => "N",
            >= 45 and < 135 => "E",
            >= 135 and < 225 => "S",
            >= 225 and < 315 => "W",
            _ => "?"
        };
        compass45.text = y switch {
            >= 270 or < 0 => "NW",
            >= 0 and < 90 => "NE",
            >= 90 and < 180 => "SE",
            >= 180 and < 270 => "SW",
            _ => "?"
        };
    }

}
