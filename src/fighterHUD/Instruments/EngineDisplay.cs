﻿using UnityEngine;

/*
 * Engine/throttle power sliders.
 */
namespace FighterHUD;
public class EngineDisplay : MonoBehaviour {

    GameObject afterburnerOn = null;
    GameObject engineOn = null;

    UnityEngine.UI.Slider throttle = null;

    void Start() {
        afterburnerOn = transform.FindChild("Afterburner").gameObject;
        engineOn = transform.FindChild("Engine").gameObject;
        throttle = transform.FindChild("Throttle Slider").GetComponent<UnityEngine.UI.Slider>();
    }

    void Update() {
        if (!CameraHolder.CurrentCamera || !CameraHolder.CurrentCamera.ShowAttitudeDisplay) return;

        CameraHolder camera = CameraHolder.CurrentCamera;

        if (!camera.IsEngineRunning) {
            throttle.value = Mathf.Lerp(throttle.value, 0, 0.2f);
        }
        else if (MachineTracker.HasThrottleHinge) {
            throttle.value = Mathf.Lerp(throttle.value, MachineTracker.ThrottleLevel, 0.2f);
        }
        else {
            float tForward = (camera.ThrottleForward.IsHeld || camera.ThrottleForward.EmulationHeld()) ? 0.5f : 0;
            float tReverse = (camera.ThrottleReverse.IsHeld || camera.ThrottleReverse.EmulationHeld()) ? 0.5f : 0;

            throttle.value = Mathf.Lerp(throttle.value, 0.5f + (tForward - tReverse), 0.2f);
        }

        afterburnerOn.SetActive(camera.ThrottleForward.IsHeld || camera.ThrottleForward.EmulationHeld());
        engineOn.SetActive(camera.IsEngineRunning);
    }
}
