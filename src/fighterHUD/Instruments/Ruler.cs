﻿using SRF;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace FighterHUD;
/// <summary>
/// Wrap-around ruler. Assumes elements are centered over 0 along the scroll axis.
/// </summary>
internal class Ruler : MonoBehaviour {

    public enum Direction { Vertical, Horizontal }

    public Direction Mode = Direction.Vertical;
    public float Value = 0;

    float wrapDistance;
    Dictionary<RectTransform, float> elements;

    void Start() {
        elements = transform.GetChildren().ToDictionary(
            e => e as RectTransform,
            e => Mode == Direction.Vertical ? e.localPosition.y : e.localPosition.x
        );
        wrapDistance = Mathf.Abs(Mode switch {
            Direction.Vertical => elements.Keys.Max(element => Mathf.Abs(element.localPosition.y)),
            Direction.Horizontal => elements.Keys.Max(element => Mathf.Abs(element.localPosition.x)),
            _ => throw new NotImplementedException($"Mode '{Mode}' not implemented")
        });
    }

    void Update() {
        foreach (var kvp in elements) {
            var item = kvp.Key;
            var originalPosition = kvp.Value;

            var newPosition = Mode switch {
                Direction.Vertical => originalPosition - Value,
                Direction.Horizontal => originalPosition - Value,
                _ => throw new NotImplementedException($"Mode '{Mode}' not implemented")
            };

            while (Mathf.Abs(newPosition) > wrapDistance) {
                if (newPosition < -wrapDistance) {
                    newPosition += wrapDistance * 2;
                }
                else if (newPosition > wrapDistance) {
                    newPosition -= wrapDistance * 2;
                }
            }
            item.localPosition = Mode switch {
                Direction.Vertical => new Vector2(item.localPosition.x, newPosition),
                Direction.Horizontal => new Vector2(newPosition, item.localPosition.y),
                _ => throw new NotImplementedException($"Mode '{Mode}' not implemented")
            };
        }
    }
}
