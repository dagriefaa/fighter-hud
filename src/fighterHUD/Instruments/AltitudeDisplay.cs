﻿using Modding;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/**
 * Display controller for altitude text, ruler, and slider.
 */
namespace FighterHUD;
public class AltitudeDisplay : MonoBehaviour {

    const float MP_CEIL_HEIGHT = 1750f;

    Ruler altitudeRuler;
    Text altitudeText;
    UnityEngine.UI.Slider altitudeSlider = null;

    public static float MaxHeight = -1;

    Transform ceiling = null;

    void Start() {
        altitudeText = GetComponent<Text>();
        altitudeRuler = CameraHUDController.Screen.FindChild("First Person Frame/Altitude Ruler").gameObject.AddComponent<Ruler>();
        altitudeRuler.Mode = Ruler.Direction.Vertical;
        altitudeSlider = CameraHUDController.Screen.FindChild("First Person Frame/Altitude Slider").GetComponent<UnityEngine.UI.Slider>();

        CameraHUDController.OnTextUpdate += () => altitudeText.text = Mathf.RoundToInt(CameraHolder.CurrentCamera.transform.position.y).ToString();
        Events.OnActiveSceneChanged += OnSceneChanged;
        OnSceneChanged(default, default);
    }

    void OnSceneChanged(Scene scene1, Scene scene2) {
        if (StatMaster.isMP) {
            ceiling = GameObject.Find("WorldBoundaryTop")?.transform;
        }
    }

    void Update() {
        if (!ReferenceMaster.activeMachineSimulating || !CameraHolder.CurrentCamera) return;

        if (CameraHolder.CurrentCamera.ShowAttitudeDisplay) {
            float y = CameraHolder.CurrentCamera.transform.position.y;
            altitudeRuler.Value = y;

            if (MaxHeight != -1) {
                altitudeSlider.value = y / MaxHeight;
            }
            else if (StatMaster.isMP) {
                altitudeSlider.value = (y + (MP_CEIL_HEIGHT - ceiling.position.y)) / (MP_CEIL_HEIGHT - 10f);
            }
            else {
                altitudeSlider.value = y / 2000f;
            }

        }
    }

}
