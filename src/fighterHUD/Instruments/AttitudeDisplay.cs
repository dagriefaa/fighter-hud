﻿using UnityEngine;

/**
 * Display controller for artificial horizon.
 */
namespace FighterHUD;
public class AttitudeDisplay : MonoBehaviour {

    Ruler attitudeImage;

    Quaternion hudRotation;

    void Start() {
        attitudeImage = gameObject.AddComponent<Ruler>();
        attitudeImage.Mode = Ruler.Direction.Vertical;

        transform.GetChild(0).gameObject.AddComponent<AttitudeLine>().offset = 0;
        transform.GetChild(1).gameObject.AddComponent<AttitudeLine>().offset = 2.5f;
        transform.GetChild(2).gameObject.AddComponent<AttitudeLine>().offset = 5f;
        transform.GetChild(3).gameObject.AddComponent<AttitudeLine>().offset = 7.5f;
        transform.GetChild(4).gameObject.AddComponent<AttitudeLine>().offset = -2.5f;
        transform.GetChild(5).gameObject.AddComponent<AttitudeLine>().offset = -5f;
    }

    void Update() {
        if (!CameraHolder.CurrentCamera) { return; }

        hudRotation = Quaternion.Euler(CameraHUDController.MainCamera.transform.rotation.eulerAngles - new Vector3(
                0f,
                CameraHolder.CurrentCamera.CameraBlock.CompositeTracker.rotation.eulerAngles.y,
                0f
            ));

        transform.localRotation = Quaternion.Euler(transform.localRotation.x, transform.localRotation.y, -hudRotation.eulerAngles.z);
        var y = -CameraHUDController.MainCamera.transform.rotation.eulerAngles.x;
        attitudeImage.Value = y / 7.5f * 240f;
    }
}
