﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace FighterHUD;
internal class AttitudeLine : MonoBehaviour {

    public float offset = 0;

    GameObject zero;
    GameObject positive;
    GameObject negative;

    List<Text> numbers;

    void Start() {
        zero = transform.FindChild("Zero")?.gameObject;
        positive = transform.FindChild("Positive").gameObject;
        negative = transform.FindChild("Negative").gameObject;

        numbers = GetComponentsInChildren<Text>(true).ToList();
    }

    void Update() {
        var cameraY = CameraHUDController.MainCamera.transform.rotation.eulerAngles.x + offset;
        if (cameraY > 180) {
            cameraY -= 360;
        }
        var cyAbs = Mathf.Abs(cameraY);
        var cySign = -Mathf.Sign(cameraY);
        float value;
        if (zero && cyAbs is < 7.5f or > 82.5f) {
            zero.SetActive(true);
            positive.SetActive(false);
            negative.SetActive(false);
            numbers.ForEach(x => x.gameObject.SetActive(false));
        }
        else {
            zero?.SetActive(false);
            value = cySign * (Mathf.Floor((cyAbs + 7.5f) / 15) * 15 + offset * cySign);
            positive.SetActive(value is > 0 and < 90);
            negative.SetActive(value is < 0 and > -90);

            var numberAngle = transform.parent.localEulerAngles.z;
            if (numberAngle > 180) {
                numberAngle -= 360;
            }
            numbers.ForEach(x => {
                x.gameObject.SetActive(value is > -90 and < 90);
                x.text = value.ToString();

                var rot = x.transform.localEulerAngles;
                rot.z = -numberAngle;
                x.transform.localEulerAngles = rot;
            });
        }
    }
}
