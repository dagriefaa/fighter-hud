using Modding;
using Modding.Blocks;
using Modding.Common;
using System.Collections.Generic;
using UnityEngine;

namespace FighterHUD;
public class MachineTracker : MonoBehaviour {

    // test
    // - singleplayer
    // - host
    // - client
    // - client local sim
    // - host spectator
    // - client spectator

    static MessageType MVelocity;
    static MessageType MCameraChange;
    static MessageType MLandingGearAngle;
    static MessageType MThrottleAngle;

    public static void SetupMessages() {

        MVelocity = ModNetworking.CreateMessageType(DataType.Vector3);
        ModNetworking.Callbacks[MVelocity] += msg => {
            OwnMachine.Velocity = (Vector3)msg.GetData(0);
        };

        MCameraChange = ModNetworking.CreateMessageType(DataType.Machine, DataType.Block);
        ModNetworking.Callbacks[MCameraChange] += msg => {
            PlayerMachine m = msg.GetData(0) as PlayerMachine;
            BlockBehaviour b = (msg.GetData(1) as Block)?.InternalObject;
            if (!Machines.TryGetValue(m.InternalObject.PlayerID, out Data data)) {
                Machines.Add(m.InternalObject.PlayerID, data = new());
            }
            data.CameraBlock = b as FixedCameraBlock;
            if (data.CameraBlock) {
                data.LastPosition = data.CameraBlock.transform.position;
            }
        };

        MLandingGearAngle = ModNetworking.CreateMessageType(DataType.Single);
        ModNetworking.Callbacks[MLandingGearAngle] += msg => {
            OwnMachine.LandingGearRetractLevel = (float)msg.GetData(0);
            OwnMachine.HasLandingGearOnHost = true;
        };

        MThrottleAngle = ModNetworking.CreateMessageType(DataType.Single);
        ModNetworking.Callbacks[MThrottleAngle] += msg => {
            OwnMachine.ThrottleLevel = (float)msg.GetData(0);
            OwnMachine.HasThrottleHingeOnHost = true;
        };
    }

    public class Data {
        public FixedCameraBlock CameraBlock = null;
        public Vector3 LastPosition = Vector3.zero;
        public Vector3 Velocity = Vector3.zero;

        public bool HasLandingGearOnHost = false;
        public SteeringWheel LandingGear = null;
        public float LandingGearRetractLevel = -1;

        public bool HasThrottleHingeOnHost = false;
        public SteeringWheel Throttle = null;
        public float ThrottleLevel = -1;
        public bool ThrottleReversed = false;

        public override string ToString() => JsonUtility.ToJson(this);
    }

    static readonly Dictionary<ushort, Data> Machines = new();

    static Data OwnMachine {
        get {
            if (!Machines.TryGetValue(Machine.Active().PlayerID, out Data data)) {
                Machines.Add(Machine.Active().PlayerID, data = new());
            }
            return data;
        }
    }

    public static Vector3 Velocity => OwnMachine.Velocity;
    public static bool HasLandingGear => OwnMachine.LandingGear || OwnMachine.HasLandingGearOnHost;
    public static bool HasThrottleHinge => OwnMachine.Throttle || OwnMachine.HasThrottleHingeOnHost;
    public static float LandingGearRetractLevel => OwnMachine.LandingGearRetractLevel;
    public static float ThrottleLevel => OwnMachine.ThrottleLevel;

    void Awake() {
        Events.OnMachineSimulationToggle += (m, sim) => {
            // reset client data on sim exit
            if (!sim && Machines.TryGetValue(m.InternalObject.PlayerID, out Data data)) {
                data.HasLandingGearOnHost = false;
                data.HasThrottleHingeOnHost = false;
            }
        };
    }


    void FixedUpdate() {
        if (Mod.SceneNotPlayable()) {
            return;
        }

        if (StatMaster.isHosting) {
            Player.GetAllPlayers()
                .FindAll(p => !p.IsSpectator && !p.InLocalSim)
                .ConvertAll(p => p.Machine.InternalObjectServer)
                .ForEach(UpdateMachine);
        }
        else if (!(StatMaster.isClient && !StatMaster.isLocalSim) && Machine.Active()) {
            UpdateMachine(Machine.Active());
        }
    }

    private void UpdateMachine(Machine m) {
        if (!m || !m.isSimulating) {
            return;
        }
        if (!Machines.TryGetValue(m.PlayerID, out Data data) || !data.CameraBlock) {
            return;
        }

        data.Velocity = (data.CameraBlock.transform.position - data.LastPosition) / Time.fixedDeltaTime;
        data.LastPosition = data.CameraBlock.transform.position;
        if (StatMaster.isHosting) {
            ModNetworking.SendTo(Player.From(m.PlayerID), MVelocity.CreateMessage(data.Velocity));
        }

        if (data.LandingGear) {
            SteeringWheel hinge = data.LandingGear;

            float level;
            if (hinge.Flipped) {
                level = hinge.AngleToBe < 0
                    ? Mathf.InverseLerp(0, -hinge.LimitsSlider.Min, hinge.AngleToBe)
                    : Mathf.InverseLerp(0, hinge.LimitsSlider.Max, hinge.AngleToBe);
            }
            else {
                level = hinge.AngleToBe > 0
                    ? Mathf.InverseLerp(0, hinge.LimitsSlider.Min, hinge.AngleToBe)
                    : Mathf.InverseLerp(0, -hinge.LimitsSlider.Max, hinge.AngleToBe);
            }

            if (StatMaster.isHosting && level != data.LandingGearRetractLevel) {
                ModNetworking.SendTo(Player.From(m.PlayerID), MLandingGearAngle.CreateMessage(level));
            }
            data.LandingGearRetractLevel = level;
        }
        if (data.Throttle) {
            SteeringWheel hinge = data.Throttle;

            var level = hinge.Flipped
                ? Mathf.InverseLerp(-hinge.LimitsSlider.Min, hinge.LimitsSlider.Max, hinge.AngleToBe)
                : Mathf.InverseLerp(hinge.LimitsSlider.Min, -hinge.LimitsSlider.Max, hinge.AngleToBe);

            if (data.ThrottleReversed) {
                level = 1 - level;
            }

            if (StatMaster.isHosting && level != data.ThrottleLevel) {
                ModNetworking.SendTo(Player.From(m.PlayerID), MThrottleAngle.CreateMessage(level));
            }
            data.ThrottleLevel = level;
        }
    }

    public static void SetCurrentCamera(FixedCameraBlock cameraBlock) {
        OwnMachine.CameraBlock = cameraBlock;
        if (OwnMachine.CameraBlock) {
            OwnMachine.LastPosition = OwnMachine.CameraBlock.transform.position;
        }
        ModNetworking.SendToHost(MCameraChange.CreateMessage(PlayerMachine.GetLocal(), Block.From(cameraBlock)));
    }

    public static void SetLandingGear(SteeringWheel hinge) {
        if (!Machines.TryGetValue(hinge.ParentMachine.PlayerID, out Data data)) {
            data = Machines[hinge.ParentMachine.PlayerID] = new();
        }
        if (data.LandingGear) {
            return;
        }
        data.LandingGear = hinge;
    }

    public static void SetThrottle(SteeringWheel hinge, bool reversed) {
        if (!Machines.TryGetValue(hinge.ParentMachine.PlayerID, out Data data)) {
            data = Machines[hinge.ParentMachine.PlayerID] = new();
        }
        if (data.Throttle) {
            return;
        }
        data.Throttle = hinge;
        data.ThrottleReversed = reversed;
    }
}