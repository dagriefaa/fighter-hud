﻿using ObjectExplorer.Mappings;
using UnityEngine;

namespace FighterHUD;

public class Mapper : MonoBehaviour {

    void Awake() {
        ObjectExplorer.ObjectExplorer.AddMappings("FighterHUD",
            new MBool<CameraHUDController>(
                "CameraHUDController.Disable: HUD Timer",
                c => c.DisableHUDTimer,
                (c, x) => c.DisableHUDTimer = x
            ),
            new MBool<CameraHUDController>(
                "CameraHUDController.Disable: Machine Icon",
                c => c.DisableMachineIcon,
                (c, x) => c.DisableMachineIcon = x
            ),
            new MBool<CameraHUDController>(
                "CameraHUDController.Disable: Icing Overlay",
                c => c.DisableIceOverlay,
                (c, x) => c.DisableIceOverlay = x
            )
        );

        Destroy(this);
    }
}
