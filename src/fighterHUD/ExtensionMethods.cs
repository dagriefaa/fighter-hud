﻿using Modding;
using UnityEngine;

namespace FighterHUD;
internal static class ExtensionMethods {

    public static Sprite ToSprite(this ModTexture texture) {
        return Sprite.Create(texture, new Rect(0, 0, texture.Texture.width, texture.Texture.height), new Vector2(0.5f, 0.5f), 100);
    }
}
