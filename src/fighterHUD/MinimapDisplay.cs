﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/**
 * Handles anything related to the minimap object.
 */
namespace FighterHUD;
public class MinimapDisplay : MonoBehaviour {

    public static readonly int MINIMAP_PIXELS = 300;
    public static readonly int FORWARD_OFFSET = 50;

    public static readonly float ZOOM_STEP = 100f;

    static float _minimapSize = 1000f;
    public static float MinimapSize {
        get {
            return _minimapSize;
        }
        set {
            _minimapSize = Mathf.Clamp(value, 200f, 4000f);
        }
    }

    public static float Scale { get { return MINIMAP_PIXELS / MinimapSize; } }

    Transform worldBounds = null;
    float boundsSize = 2000f;

    RectTransform worldBoundsDisplay;
    Text minimapSizeText;
    List<Transform> directions = new List<Transform>();

    void Awake() {
        worldBoundsDisplay = this.transform.FindChild("Boundaries").gameObject.GetComponent<RectTransform>();
        minimapSizeText = this.transform.parent.FindChild("Size Text").gameObject.GetComponent<Text>();
        directions.Add(this.transform.FindChild("N").transform);
        directions.Add(this.transform.FindChild("S").transform);
        directions.Add(this.transform.FindChild("E").transform);
        directions.Add(this.transform.FindChild("W").transform);
    }

    void Update() {
        if (!CameraHolder.CurrentCamera) {
            return;
        }
        this.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, CameraHUDController.MainCamera.transform.rotation.eulerAngles.y));
        directions.ForEach(x => { x.localEulerAngles = this.transform.localEulerAngles * -1f; });

        if (SceneManager.GetActiveScene().name != "MISTY MOUNTAIN") {
            worldBoundsDisplay.gameObject.SetActive(true);
            if (!worldBounds) {
                if (SceneManager.GetActiveScene().name == "SANDBOX") {
                    worldBounds = GameObject.Find("WORLD BOUNDARIES_LARGE (1)").transform.GetChild(0);
                }
                else if (SceneManager.GetActiveScene().name == "BARREN EXPANSE") {
                    worldBounds = GameObject.Find("WORLD BOUNDARIES_LARGE").transform.GetChild(0);
                }
                else {
                    worldBounds = GameObject.Find("WORLD BOUNDARIES").transform.GetChild(0);
                }
                boundsSize = Mathf.Max(Mathf.Abs(worldBounds.position.x), Mathf.Abs(worldBounds.position.z)) * 2f;
            }

            worldBoundsDisplay.sizeDelta = Vector2.one * boundsSize * MinimapDisplay.Scale;
            worldBoundsDisplay.localPosition = new Vector3(
                (-CameraHolder.CurrentCamera.transform.position.x),
                (-CameraHolder.CurrentCamera.transform.position.z),
                0f) * MinimapDisplay.Scale;
        }
        else {
            worldBoundsDisplay.gameObject.SetActive(false);
        }

        float zoom = InputManager.ZoomValue();
        if (zoom != 0f && CameraHolder.CurrentCamera.CameraBlock.CamMode == FixedCameraBlock.Mode.FirstPerson) {
            MinimapSize += zoom * ZOOM_STEP * 10f;
            minimapSizeText.text = $"{MinimapSize:0.}m";
        }
        else if (CameraHolder.CurrentCamera.CameraBlock.CamMode != FixedCameraBlock.Mode.FirstPerson) {
            MinimapSize = 1000f;
            minimapSizeText.text = $"1000m";
        }
    }
}
