﻿using UnityEngine;
using System.Collections;
using Modding.Common;

/*
 * Multiplayer team progress display.
 */
namespace FighterHUD;
public class MultiProgressDisplay : ProgressDisplay {

    void Awake() {
        ProgressOrderController.TeamList.Add(this);
    }

    protected override void Update() {
        ProgressText.enabled = StatMaster.isMP && Player.GetLocalPlayer().Team != team && ProgressSlider.value > 0f;
        base.Update();
    }
}
