﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Modding.Common;

/*
 * Player progress bar display (MP and SP).
 */
namespace FighterHUD;
public class MainProgressDisplay : ProgressDisplay {

    GameObject background;
    GameObject fillArea;
    public GameObject label = null;

    void Awake() {
        background = this.transform.FindChild("Background").gameObject;
        fillArea = this.transform.FindChild("Fill Area").gameObject;
    }

    protected override void Update() {
        team = Player.GetLocalPlayer()?.Team ?? MPTeam.None;

        bool isActive = false;
        if (StatMaster.isMP) {
            for (MPTeam i = MPTeam.None; i < MPTeam.Blue; i++) {
                isActive |= Progress(i) > 0f;
            }
        }
        else {
            isActive = Progress(MPTeam.None) > 0f;
        }
        background.SetActive(isActive);
        fillArea.SetActive(isActive);
        label.SetActive(isActive);
        ProgressText.enabled = isActive;

        ProgressText.color = ProgressSlider.fillRect.GetComponent<Image>().color = team switch {
            MPTeam.None => Color.white,
            MPTeam.Red => Color.red,
            MPTeam.Green => Color.green,
            MPTeam.Orange => Color.yellow,
            MPTeam.Blue => Color.blue,
            _ => Color.white,
        };
        base.Update();

    }
}
