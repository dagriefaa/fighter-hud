﻿using UnityEngine;
using UnityEngine.UI;

/*
 * Displays 100% time elapsed in simulation.
 */
namespace FighterHUD;
public class SimulationTimeDisplay : MonoBehaviour {

    public Text SimTimeText { get; private set; } = null;

    // Use this for initialization
    public void Start() {
        SimTimeText = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update() {
        if (!CameraHolder.CurrentCamera) return;

        var time = CameraHUDController.SimTimer.Elapsed;
        int m = Mathf.FloorToInt(time.Minutes);
        int s = Mathf.FloorToInt(time.Seconds);
        int ms = Mathf.RoundToInt(time.Milliseconds);
        SimTimeText.text = $"TIME  {m:00}:{s:00}:{ms:000}";
    }
}
