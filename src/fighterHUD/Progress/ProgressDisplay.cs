﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Modding.Blocks;

/*
 * Progress display superclass.
 */
namespace FighterHUD;
public class ProgressDisplay : MonoBehaviour {

    public UnityEngine.UI.Slider ProgressSlider { get; private set; } = null;
    public Text ProgressText { get; private set; } = null;

    protected static WinCondition winCondition = null;

    public MPTeam team = MPTeam.None;
    public float CurrentProgress { get { return Progress(team); } }

    void Start() {
        ProgressSlider = GetComponent<UnityEngine.UI.Slider>();
        ProgressText = GetComponent<Text>();

        if (!winCondition) winCondition = GameObject.FindObjectOfType<WinCondition>();
    }

    // Update is called once per frame
    protected virtual void Update() {
        ProgressSlider.value = Progress(team) / 100f;
        ProgressText.text = "" + Progress(team).ToString("0") + "%";
    }

    public static float Progress(MPTeam team) {
        if (team == MPTeam.None) {
            float noneProgress = Mathf.Clamp((StatMaster.isMP) ? winCondition.GetTeamProgress(MPTeam.None) : DestructionBar.Instance.fullPercent, 0f, 100f);
            return ((WinCondition.hasWon) ? 100f : noneProgress);
        }
        return winCondition.GetTeamProgress(team);
    }
}
