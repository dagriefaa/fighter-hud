﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

/*
 * Orders multiplayer progress bars.
 */
namespace FighterHUD;
public class ProgressOrderController : MonoBehaviour {

    public static List<ProgressDisplay> TeamList = new List<ProgressDisplay>();

    // Update is called once per frame
    void Update() {
        if (StatMaster.isMP) {
            TeamList = TeamList.OrderByDescending(p => p.CurrentProgress).ToList();
            for (int i = 0; i < TeamList.Count; i++) {
                TeamList[i].transform.SetSiblingIndex(i);
            }
        }
    }
}
