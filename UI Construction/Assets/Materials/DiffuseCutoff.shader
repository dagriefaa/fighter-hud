﻿Shader "Custom/DiffuseCutoff" {
	
	Properties {
        _Color("Color",Color) = (0,0,1,0.2)
        _DiffuseOverlaps("Overlaps",int)=6
	}

	SubShader {
	    Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
	    LOD 200

        Stencil {
            Ref [_DiffuseOverlaps]
            Comp Greater
            Pass IncrSat 
            Fail IncrSat 
        }

		CGPROGRAM
		#pragma surface surf Lambert alpha:fade

		sampler2D _MainTex;
		fixed4 _Color;

		struct Input {
		    float2 uv_MainTex;
		};

		void surf (Input IN, inout SurfaceOutput o) {
		    fixed4 c = _Color;
		    o.Albedo = c.rgb;
		    o.Alpha = c.a;
		}
		ENDCG
	}
}
